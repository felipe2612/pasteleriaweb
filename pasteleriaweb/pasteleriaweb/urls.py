"""pasteleriaweb URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from estructura import views

urlpatterns = [
    path('admin/', admin.site.urls),
    # MIS RUTAS 
    path('', views.index, name="index"),
    path('Tienda/', views.comprar, name="Tienda" ),
    path('Tienda/datoCliente/', views.dato_cliente, name="datoCliente"),
    path('Locacion/', views.locacion, name="Locacion"),
    path('Tienda/Set1/', views.set1, name="Set1"),
    path('Tienda/Set2/', views.set2, name="Set2"),
    path('Tienda/Set3/', views.set3, name="Set3"),
    path('Tienda/Set4/', views.set4, name="Set4"),
    path('Tienda/Set5/', views.set5, name="Set5"),
    path('Tienda/Set6/', views.set6, name="Set6"),
    path('Tienda/Set7/', views.set7, name="Set7"),
    path('Tienda/Set8/', views.set8, name="Set8"),
    path('Tienda/Set9/', views.set9, name="Set9"),
    path('Tienda/Set10/', views.set10, name="Set10"),
    path('Tienda/Set11/', views.set11, name="Set11"),
    path('Tienda/Set12/', views.set12, name="Set12"),
    #path('Login/', views.login, name="Login"),
    #path('Login/Registro/', views.registro, name="Registro"),

    # LOGIN Y REGISTRO DE USUARIO : 
    path('registro/', views.registro, name="registro"),
    path('login/',views.user_login, name="login"),
    path('logout/',views.Logout_user, name="logout"),
 
    # PAGINA ADMINISTRATIVA (CRUD): 
    path('MainAdmin/', views.mainAdmin, name="MainAdmin"),
    path('Ingresar/', views.Ingresar, name="Ingresar"),
    path('Modificar/', views.Modificar, name="Modificar"),
    path('Eliminar/', views.Eliminar, name="Eliminar"),
    path('Listar/', views.Listar, name="Listar"),

]
