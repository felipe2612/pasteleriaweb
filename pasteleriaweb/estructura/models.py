from django.db import models

# Create your models here.

class Producto(models.Model):
    nombre = models.CharField(max_length=60)
    precio = models.IntegerField()
    descripcion = models.CharField(max_length=100)
    cantidad = models.IntegerField()
    fechaCreacion = models.DateTimeField(auto_now_add=True)

class DatosCliente(models.Model):
    email = models.CharField(max_length=150)
    telefono = models.CharField(max_length=15)
    fechaCreacion = models.DateTimeField(auto_now_add=True)
